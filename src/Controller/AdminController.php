<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Form\TestForm;
use App\Shell\TestShell;

/**
 * Admin Controller
 *
 * @property \App\Model\Table\AdminTable $Admin
 */
class AdminController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
	$message = '';
	$post = $this->request->data;

	if (array_key_exists('submit1', $post)) {

		$shell = new TestShell();
		$shell->startup();
		$shell->main();
		$message = 'Submit1 実行';

	} else if (array_key_exists('submit2', $post)) {

		$shell = new TestShell();
		$shell->startup();
		$shell->main();
		$message = 'Submit2 実行';

	}


	if (!empty($message)) {
	    $this->Flash->success($message);
	}

	$contact = new TestForm();
	if (!$contact->execute($this->request->data)) {
	    $this->Flash->error(__('error'));
	}
	$this->set('contact', $contact);
    }

    /**
     * View method
     *
     * @param string|null $id Admin id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
	$admin = $this->Admin->get($id, [
	    'contain' => []
	]);

	$this->set('admin', $admin);
	$this->set('_serialize', ['admin']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
	$admin = $this->Admin->newEntity();
	if ($this->request->is('post')) {
	    $admin = $this->Admin->patchEntity($admin, $this->request->data);
	    if ($this->Admin->save($admin)) {
		$this->Flash->success(__('The admin has been saved.'));

		return $this->redirect(['action' => 'index']);
	    }
	    $this->Flash->error(__('The admin could not be saved. Please, try again.'));
	}
	$this->set(compact('admin'));
	$this->set('_serialize', ['admin']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Admin id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
	$admin = $this->Admin->get($id, [
	    'contain' => []
	]);
	if ($this->request->is(['patch', 'post', 'put'])) {
	    $admin = $this->Admin->patchEntity($admin, $this->request->data);
	    if ($this->Admin->save($admin)) {
		$this->Flash->success(__('The admin has been saved.'));

		return $this->redirect(['action' => 'index']);
	    }
	    $this->Flash->error(__('The admin could not be saved. Please, try again.'));
	}
	$this->set(compact('admin'));
	$this->set('_serialize', ['admin']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Admin id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
	$this->request->allowMethod(['post', 'delete']);
	$admin = $this->Admin->get($id);
	if ($this->Admin->delete($admin)) {
	    $this->Flash->success(__('The admin has been deleted.'));
	} else {
	    $this->Flash->error(__('The admin could not be deleted. Please, try again.'));
	}

	return $this->redirect(['action' => 'index']);
    }
}
